import cv2  # for capturing videos
import math  # for mathematical operations
import matplotlib.pyplot as plt  # for plotting the images
# %matplotlib inline
import pandas as pd
from keras.preprocessing import image  # for preprocessing the images
import numpy as np  # for mathematical operations
from keras.utils import np_utils
from skimage.transform import resize  # for resizing images
from sklearn.model_selection import train_test_split
from glob import glob
from tqdm import tqdm

# Opens trainlist01.txt for reading => splits names at new line => close file
f = open("C:/Users/siVm01/Desktop/Team3/ucfTrainTestlist/trainlist01.txt", "r")
temp = f.read()
videos = temp.split('\n')
f.close()
# print(videos)

# Appends video names to DataFrame
train = pd.DataFrame()
train['video_name'] = videos
# print(train['video_name'])
train = train[:-1]
train.head()
# print(train['video_name'])

# Opens testlist01.txt for reading => splits names at new line => close file
f = open("C:/Users/siVm01/Desktop/Team3/ucfTrainTestlist/testlist01.txt", "r")
tmp = f.read()
testListVideos = tmp.split('\n')
f.close()

# Appends video names to new DataFrame
test = pd.DataFrame()
test['video_name'] = testListVideos
test = test[:-1]
test.head()

# train_video_tag stores the category
train_video_tag = []
for i in range(train.shape[0]):
    train_video_tag.append(train['video_name'][i].split('/')[0])
    # print(train_video_tag)


train['tag'] = train_video_tag
# print(train['tag'])

# test_video_tag stores the video category
test_video_tag = []
for i in range(test.shape[0]):
    #print(test['video_name'][i].split('/')[0])
    test_video_tag.append(test['video_name'][i].split('/')[0])

test['tag'] = test_video_tag

# For the size of train => set videoFile name => create video capture path => create cv2 video capture from path =>
# set frames captured to 5 => initialize filename path => write image arg contents to the filename path arg
for i in range(train.shape[0]):
    count = 0
    videoFile = train['video_name'][i]
    vidCap = "C:/Users/siVm01/Desktop/Team3/UCF/" + videoFile.split(' ')[0]
    vidCap = cv2.VideoCapture(vidCap)
    frameRate = vidCap.get(5)
    x = 1

    # Removed use of count in naming convention of frame due to numbering issues
    # Naming now uses i from for loop
    # if success != True is not needed but can be used to debug with print() statements
    while vidCap.isOpened():
        print('Back to beginning of while loop')
        frameId = vidCap.get(1)
        success, image = vidCap.read()
        if success != True:
            break
        if frameId % math.floor(frameRate) == 0:
            print('Writing to file...')
            filename = "C:/Users/siVm01/Desktop/Team3/train_1/" + videoFile.split('/')[1].split(' ')[0] + \
                       "_frame" + str(i) + ".jpg"
            print(filename)
            cv2.imwrite(filename, image)
        #count+=1
        vidCap.release()

