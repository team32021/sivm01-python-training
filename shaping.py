import keras
from keras.models import Sequential
from keras.applications.vgg16 import VGG16
from keras.layers import Dense, InputLayer, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D, GlobalMaxPooling2D
from keras.preprocessing import image
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm
from sklearn.model_selection import train_test_split

# Reading CVS from Team3 directory
train = pd.read_csv('C:/Users/siVm01/Desktop/Team3/train_new.csv')
train.head()
train_img = []

# For the size of train => load each image frame path => adjust frame sizing => add to train_img array
for i in tqdm(range(train.shape[0])):
    # print(train['image'][i])
    img = image.load_img('C:/Users/siVm01/Desktop/Team3/train_1/' + train['image'][i], target_size=(224, 224, 3))
    img = image.img_to_array(img)
    img = img / 255
    train_img.append(img)


X = np.array(train_img)
X.shape
print(X)
