import cv2  # for capturing videos
import math  # for mathematical operations
import matplotlib.pyplot as plt  # for plotting the images
# %matplotlib inline
import pandas as pd
from keras.preprocessing import image  # for preprocessing the images
import numpy as np  # for mathematical operations
from keras.utils import np_utils
from skimage.transform import resize  # for resizing images
from sklearn.model_selection import train_test_split
from glob import glob
from tqdm import tqdm

# Accessor for all JPG files in path
images = glob("C:/Users/siVm01/Desktop/Team3/train_1/*.jpg")
# Image Name, Image Class (eg. YoYo, Makeup, Benchpress)
train_image = []
train_class = []

# For the number of images => add image path to train_image => add image classification to train_class
for i in tqdm(range(len(images))):
    print('Image Name\t\t\t\tClass Name')
    print(images[i].split('\\')[1] + '\t' + images[i].split("\\")[1].split('_')[1])
    print('=================================================================')
    train_image.append(images[i].split('\\')[1])
    train_class.append(images[i].split("\\")[1].split('_')[1])


# Data mapping for train_image and train_class into one DataFrame
train_data = pd.DataFrame()
train_data['image'] = train_image
train_data['class'] = train_class
print(train_data['image'])

# Converts DataFrame into CSV file
# Headers = image,class
train_data.to_csv("C:/Users/siVm01/Desktop/Team3/train_new.csv", header=True, index=False)



